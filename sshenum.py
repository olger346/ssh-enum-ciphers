import socket
import paramiko

host = "192.168.1.109"
port = 22

#se crea un socket de tipo ipv4 y de tipo TCP
sock = socket.socket()
def getsshconf(host, port, sock=sock):
	#conecta con el serer
	print("[+]Conectando a " + host)
	try:
		sock.connect((host, port))
		#Como se desea evaluar la capa de transporte se toma este objeto de paramiko para evaluar ciphers
		#intercambio de llaves y demas
		transport = paramiko.transport.Transport(sock)
		transport.start_client(event=None, timeout=None)
		banner = transport.get_banner()
		ciphers_algorithm = paramiko.transport.SecurityOptions(transport)
		cifrados = ciphers_algorithm.ciphers
		key = ciphers_algorithm.kex
		hashes = ciphers_algorithm.digests
		ktype = ciphers_algorithm.key_types
		print("banner:" + " " + str(banner) + " " + str(sock))
		print("cifrados soportados")
		for cip in cifrados:
			print(cip)
		print("")
		print("Algoritmos de intercambio de llaves")
		for kex in key:
			print(kex)
		print("")
		print("Algoritmos de clave publica")
		for kty in ktype:
			print(kty)
		#print("cifrados:" + str(ciphers_algorithm.ciphers()))
		#print("server key:" + str(key))
	finally:
		print("[+]Se ha ejecutado el modulo correctamente")

getsshconf(host,port)